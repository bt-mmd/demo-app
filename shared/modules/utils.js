export const
    delay = ms => new Promise(r => setTimeout(r, ms)),
    compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args))),
    LazyComponent = path => import(/* webpackChunkName: 'dyn-' */ '../components/' + path)