import EventEmitter from 'eventemitter3'
import {fromEvent} from 'most'

export const hub = new EventEmitter

export const app$ = fromEvent('APP', hub).multicast()
export const push$ = fromEvent('PUSH', hub).multicast()

