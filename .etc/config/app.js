export const
    PORT = 8585,
    InitProps = Object.create(null),
    InitAppData = {
        title: 'demo-app',
        lang: 'en',
        charset: 'utf-8',
        charCode: 9763,
        theme: {color: '#36393E'}
    }
