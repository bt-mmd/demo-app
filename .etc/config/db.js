export const
    dbName = 'db.json',
    dbId = 'id',
    dbCollOpts = {unique: [dbId]},
    dbOpts = {
        env: 'MEMORY',
        verbose: true
    }